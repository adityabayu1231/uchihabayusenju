@extends('layouts.app')
@section('title', 'Variants')
@section('header', 'Kelola Variant')
@section('content')
    {{-- Form isi variant --}}
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="p-8">
                    @if (session('success'))
                        <div class="bg-green-500 text-white p-4 mb-4">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="bg-red-500 text-white p-4 mb-4">
                            {{ session('error') }}
                        </div>
                    @endif
                    <h2 class="text-xl font-semibold mb-4" id="formTitle">Form Tambah Kategori Product</h2>
                    <form action="{{ route('variants.store') }}" method="POST" id="variantForm">
                        @csrf
                        <input type="hidden" id="variantId" name="variantId" value="">
                        <div class="mb-4">
                            <label for="name" class="block text-gray-700 text-sm font-bold mb-2">Nama Variant : </label>
                            <input type="text" id="name" name="name"
                                class="w-full px-3 py-2 border border-gray-300 rounded focus:outline-none focus:border-indigo-500"
                                value="" placeholder="Masukkan nama variant">
                        </div>
                        <div class="mb-4">
                            <label for="size" class="block text-gray-700 text-sm font-bold mb-2">Ukuran Variant :
                            </label>
                            <input type="text" id="size" name="size"
                                class="w-full px-3 py-2 border border-gray-300 rounded focus:outline-none focus:border-indigo-500"
                                value="" placeholder="Masukkan Ukuran (S/L/XL dll)">
                        </div>
                        <button type="submit"
                            class="bg-red-500 hover:bg-red-600 text-white font-medium py-2 px-4 rounded-md"
                            id="submitButton">Simpan Data</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- Table Variant --}}
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-6">
                <div class="p-4">
                    <h2 class="text-xl font-semibold mb-4">Daftar Variants</h2>
                    <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
                        <table class="w-full text-sm text-left">
                            <thead class="text-xs bg-indigo-500 text-white">
                                <tr>
                                    <th scope="col" class="px-6 py-3">
                                        ID
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Nama Variant
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Ukuran Variant
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($variants as $index => $variant)
                                    <tr
                                        class="{{ $index % 2 === 0 ? 'bg-gray-100' : 'bg-white' }} border-b hover:bg-gray-200">
                                        <td class="px-6 py-4">
                                            {{ $variant->id }}
                                        </td>
                                        <td class="px-6 py-4">
                                            {{ $variant->name }}
                                        </td>
                                        <td class="px-6 py-4">
                                            {{ $variant->size }}
                                        </td>
                                        <td class="px-6 py-4">
                                            <button type="button"
                                                onclick="editVariant('{{ $variant->id }}',
                                                '{{ $variant->name }}', '{{ $variant->size }}')"
                                                class="text-blue-500 hover:text-blue-700">Edit</button>
                                            <form action="{{ route('variants.destroy', $variant) }}"
                                                onclick="return confirmDelete()" method="POST" class="inline">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit"
                                                    class="text-red-500 hover:text-red-700">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                                @if ($variants->isEmpty())
                                    <tr>
                                        <td colspan="10" class="px-6 py-4 text-center text-gray-500">
                                            No data found.
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        {{ $variants->links() }}
                    </div>

                    <!-- Additional messages or warnings -->
                </div>
            </div>
        </div>
    </div>

    {{-- Script --}}
    <script>
        function editVariant(id, name, size) {
            document.getElementById('name').value = name;
            document.getElementById('size').value = size;
            document.getElementById('variantId').value = id;
            document.getElementById('formTitle').innerText = 'Ubah Variant';
            document.getElementById('submitButton').innerText = 'Ubah Variant';
            // Setelah mengisi formulir, atur metode formulir menjadi PUT
            document.getElementById('variantForm').method = 'POST';
            document.getElementById('variantForm').action = '{{ route('variants.update', '') }}/' + id;

            // Tambahkan input tersembunyi untuk metode PUT
            var methodField = document.createElement('input');
            methodField.setAttribute('type', 'hidden');
            methodField.setAttribute('name', '_method');
            methodField.setAttribute('value', 'PUT');

            // Periksa apakah input _method sudah ada, jika ya, hapus
            var existingMethodField = document.getElementById('_method');
            if (existingMethodField) {
                existingMethodField.parentNode.removeChild(existingMethodField);
            }

            // Tambahkan input _method ke dalam formulir
            document.getElementById('variantForm').appendChild(methodField);
        }
    </script>
@endsection
