<!-- Footer -->
<footer class="bg-gray-200 dark:bg-gray-700 py-8">
    <div class="container mx-auto text-center text-gray-600 dark:text-gray-300">
        <p>&copy; 2023 Laravel Shop. All rights reserved.</p>
    </div>
</footer>
<script>
    // Fungsi untuk mendapatkan gambar acak dari Unsplash
    async function getRandomImage() {
        const response = await fetch('https://source.unsplash.com/1600x900/?shopping');
        return response.url;
    }

    // Fungsi untuk mengatur latar belakang dengan gambar acak
    async function setRandomBackground() {
        const backgroundImage = await getRandomImage();
        document.getElementById('jumbotron-section').style.backgroundImage = `url('${backgroundImage}')`;
    }

    // Panggil fungsi setRandomBackground ketika halaman dimuat ulang
    window.addEventListener('load', setRandomBackground);
</script>
