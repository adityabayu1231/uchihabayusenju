<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    @vite(['resources/css/app.css', 'resources/js/app.js'])
    <title>@yield('title', 'Selamat Datang')</title>
    <style>
        .dynamic-bg {
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;
        }
    </style>
</head>

<body class="font-sans bg-gray-100 dark:bg-gray-800 text-gray-800 dark:text-white">

    <!-- Navbar -->
    @include('layouts.navbar')
    <!-- header -->
    <header class="bg-white shadow">
        <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
            <h2 class="font-semibold text-xl text-gray-800 leading-tight">
                @yield('header')
            </h2>
        </div>
    </header>
    <!-- Content -->
    <div>
        @yield('content')
    </div>
    <!-- Footer -->
    @include('layouts.footer')
</body>

</html>
