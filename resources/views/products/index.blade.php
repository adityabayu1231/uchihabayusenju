@extends('layouts.app')
@section('title', 'Product')
@section('header', 'Kelola Produk')
@section('content')
    {{-- Form Tambah/Edit Product --}}
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="p-8">
                    @if (session('success'))
                        <div class="bg-green-500 text-white p-4 mb-4">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="bg-red-500 text-white p-4 mb-4">
                            {{ session('error') }}
                        </div>
                    @endif
                    <h2 class="text-xl font-semibold mb-4" id="formTitle">Form Tambah Produk</h2>
                    <form action="{{ route('products.store') }}" method="POST" id="productForm"
                        enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" id="productId" name="productId" value="">
                        <div class="mb-4">
                            <label for="name" class="block text-gray-700 text-sm font-bold mb-2">Nama Produk:</label>
                            <input type="text" id="name" name="name"
                                class="w-full px-3 py-2 border border-gray-300 rounded focus:outline-none focus:border-indigo-500"
                                value="" placeholder="Masukkan nama produk">
                        </div>
                        <div class="mb-4">
                            <label for="description" class="block text-gray-700 text-sm font-bold mb-2">Deskripsi
                                Produk:</label>
                            <textarea id="description" name="description"
                                class="w-full px-3 py-2 border border-gray-300 rounded focus:outline-none focus:border-indigo-500"
                                placeholder="Masukkan deskripsi produk"></textarea>
                        </div>
                        <div class="mb-4">
                            <label for="price" class="block text-gray-700 text-sm font-bold mb-2">Harga:</label>
                            <input type="number" id="price" name="price"
                                class="w-full px-3 py-2 border border-gray-300 rounded focus:outline-none focus:border-indigo-500"
                                value="" placeholder="Masukkan harga produk">
                        </div>
                        <div class="mb-4">
                            <label for="stock" class="block text-gray-700 text-sm font-bold mb-2">Stok:</label>
                            <input type="number" id="stock" name="stock"
                                class="w-full px-3 py-2 border border-gray-300 rounded focus:outline-none focus:border-indigo-500"
                                value="" placeholder="Masukkan jumlah stok">
                        </div>
                        <div class="mb-4">
                            <label for="variant_id" class="block text-gray-700 text-sm font-bold mb-2">Variant:</label>
                            <select id="variant_id" name="variant_id"
                                class="w-full px-3 py-2 border border-gray-300 rounded focus:outline-none focus:border-indigo-500">
                                <option value="" selected disabled>Pilih Variant</option>
                                @foreach ($variants as $variant)
                                    <option value="{{ $variant->id }}">{{ $variant->name }} - {{ $variant->size }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-4">
                            <label for="category_id" class="block text-gray-700 text-sm font-bold mb-2">Kategori
                                Produk:</label>
                            <select id="category_id" name="category_id"
                                class="w-full px-3 py-2 border border-gray-300 rounded focus:outline-none focus:border-indigo-500">
                                <option value="" selected disabled>Pilih Kategori</option>
                                @foreach ($categories as $category)
                                    <option value="{{ $category->id }}">{{ $category->name }}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="mb-4">
                            <label class="block mb-2 text-sm font-medium text-gray-700" for="file_input">Upload
                                Gambar:</label>
                            <input
                                class="block w-full text-sm text-gray-700 border border-gray-300 rounded-lg cursor-pointer bg-gray-50 focus:outline-none"
                                id="file_input" type="file" name="image" onchange="previewImage(this)">
                        </div>
                        <div id="imagePreview" class="mb-4">
                            <!-- Tempat untuk menampilkan preview gambar -->
                        </div>
                        <button type="submit"
                            class="bg-red-500 hover:bg-red-600 text-white font-medium py-2 px-4 rounded-md"
                            id="submitButton">Simpan Data</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- Table Produk --}}
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-6">
                <div class="p-4">
                    <h2 class="text-xl font-semibold mb-4">Daftar Produk</h2>
                    <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
                        <table class="w-full text-sm text-left">
                            <thead class="text-xs bg-indigo-500 text-white">
                                <tr>
                                    <th scope="col" class="px-6 py-3">
                                        ID
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Nama Produk
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Harga
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Stok
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($products as $index => $product)
                                    <tr
                                        class="{{ $index % 2 === 0 ? 'bg-gray-100' : 'bg-white' }} border-b hover:bg-gray-200">
                                        <td class="px-6 py-4">
                                            {{ $product->id }}
                                        </td>
                                        <td class="px-6 py-4">
                                            {{ $product->name }}
                                        </td>
                                        <td class="px-6 py-4">
                                            {{ $product->price }}
                                        </td>
                                        <td class="px-6 py-4">
                                            {{ $product->stock }}
                                        </td>
                                        <td class="px-6 py-4">
                                            <button type="button"
                                                onclick="editProduct('{{ $product->id }}', '{{ $product->name }}', '{{ $product->description }}', '{{ $product->price }}', '{{ $product->stock }}', '{{ $product->variant_id }}', '{{ $product->category_id }}')"
                                                class="text-blue-500 hover:text-blue-700">Edit</button>
                                            <form action="{{ route('products.destroy', $product) }}"
                                                onclick="return confirmDelete()" method="POST" class="inline">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit"
                                                    class="text-red-500 hover:text-red-700">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                                @if ($products->isEmpty())
                                    <tr>
                                        <td colspan="10" class="px-6 py-4 text-center text-gray-500">
                                            No data found.
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        {{ $products->links() }}
                    </div>

                    <!-- Additional messages or warnings -->
                </div>
            </div>
        </div>
    </div>

    {{-- Script --}}
    <script>
        function editProduct(id, name, description, price, stock, variant_id, category_id) {
            document.getElementById('name').value = name;
            document.getElementById('description').value = description;
            document.getElementById('price').value = price;
            document.getElementById('stock').value = stock;
            document.getElementById('variant_id').value = variant_id;
            document.getElementById('category_id').value = category_id;
            document.getElementById('productId').value = id;
            document.getElementById('formTitle').innerText = 'Ubah Produk';
            document.getElementById('submitButton').innerText = 'Ubah Produk';
            // Setelah mengisi formulir, atur metode formulir menjadi PUT
            document.getElementById('productForm').method = 'POST';
            document.getElementById('productForm').action = '{{ route('products.update', '') }}/' + id;

            // Tambahkan input tersembunyi untuk metode PUT
            var methodField = document.createElement('input');
            methodField.setAttribute('type', 'hidden');
            methodField.setAttribute('name', '_method');
            methodField.setAttribute('value', 'PUT');

            // Periksa apakah input _method sudah ada, jika ya, hapus
            var existingMethodField = document.getElementById('_method');
            if (existingMethodField) {
                existingMethodField.parentNode.removeChild(existingMethodField);
            }

            // Tambahkan input _method ke dalam formulir
            document.getElementById('productForm').appendChild(methodField);
        }

        function previewImage(input) {
            var preview = document.getElementById('imagePreview');
            preview.innerHTML = ''; // Membersihkan preview sebelum menampilkan yang baru

            if (input.files && input.files[0]) {
                var reader = new FileReader();

                reader.onload = function(e) {
                    var image = document.createElement('img');
                    image.src = e.target.result;
                    image.className = 'rounded-lg mt-2'; // Memberi sedikit gaya pada gambar
                    image.style.maxWidth = '200px'; // Atur lebar maksimum
                    image.style.maxHeight = '200px'; // Atur tinggi maksimum
                    preview.appendChild(image);
                };

                reader.readAsDataURL(input.files[0]);
            }
        }
    </script>
@endsection
