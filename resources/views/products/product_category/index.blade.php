@extends('layouts.app')
@section('title', 'Category Product')
@section('header', 'Kelola Kategori')
@section('content')
    {{-- Form isi variant --}}
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg">
                <div class="p-8">
                    @if (session('success'))
                        <div class="bg-green-500 text-white p-4 mb-4">
                            {{ session('success') }}
                        </div>
                    @endif

                    @if (session('error'))
                        <div class="bg-red-500 text-white p-4 mb-4">
                            {{ session('error') }}
                        </div>
                    @endif
                    <h2 class="text-xl font-semibold mb-4" id="formTitle">Form Tambah Kategori Product</h2>
                    <form action="{{ route('category.store') }}" method="POST" id="categoryForm">
                        @csrf
                        <input type="hidden" id="categoryId" name="categoryId" value="">
                        <div class="mb-4">
                            <label for="name" class="block text-gray-700 text-sm font-bold mb-2">Nama Variant : </label>
                            <input type="text" id="name" name="name"
                                class="w-full px-3 py-2 border border-gray-300 rounded focus:outline-none focus:border-indigo-500"
                                value="" placeholder="Masukkan nama variant">
                        </div>
                        <button type="submit"
                            class="bg-red-500 hover:bg-red-600 text-white font-medium py-2 px-4 rounded-md"
                            id="submitButton">Simpan Data</button>
                    </form>
                </div>
            </div>
        </div>
    </div>

    {{-- Table Variant --}}
    <div class="py-12">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-hidden shadow-xl sm:rounded-lg p-6">
                <div class="p-4">
                    <h2 class="text-xl font-semibold mb-4">Daftar Kategori Produk</h2>
                    <div class="relative overflow-x-auto shadow-md sm:rounded-lg">
                        <table class="w-full text-sm text-left">
                            <thead class="text-xs bg-indigo-500 text-white">
                                <tr>
                                    <th scope="col" class="px-6 py-3">
                                        ID
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Nama Kategori
                                    </th>
                                    <th scope="col" class="px-6 py-3">
                                        Action
                                    </th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($productcategories as $index => $category)
                                    <tr
                                        class="{{ $index % 2 === 0 ? 'bg-gray-100' : 'bg-white' }} border-b hover:bg-gray-200">
                                        <td class="px-6 py-4">
                                            {{ $category->id }}
                                        </td>
                                        <td class="px-6 py-4">
                                            {{ $category->name }}
                                        </td>
                                        <td class="px-6 py-4">
                                            <button type="button"
                                                onclick="editVariant('{{ $category->id }}',
                                                '{{ $category->name }}')"
                                                class="text-blue-500 hover:text-blue-700">Edit</button>
                                            <form action="{{ route('category.destroy', $category) }}"
                                                onclick="return confirmDelete()" method="POST" class="inline">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit"
                                                    class="text-red-500 hover:text-red-700">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach

                                @if ($productcategories->isEmpty())
                                    <tr>
                                        <td colspan="10" class="px-6 py-4 text-center text-gray-500">
                                            No data found.
                                        </td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        {{ $productcategories->links() }}
                    </div>

                    <!-- Additional messages or warnings -->
                </div>
            </div>
        </div>
    </div>

    {{-- Script --}}
    <script>
        function editVariant(id, name, size) {
            document.getElementById('name').value = name;
            document.getElementById('size').value = size;
            document.getElementById('categoryId').value = id;
            document.getElementById('formTitle').innerText = 'Ubah Kategori';
            document.getElementById('submitButton').innerText = 'Ubah Kategori';
            // Setelah mengisi formulir, atur metode formulir menjadi PUT
            document.getElementById('categoryForm').method = 'POST';
            document.getElementById('categoryForm').action = '{{ route('variants.update', '') }}/' + id;

            // Tambahkan input tersembunyi untuk metode PUT
            var methodField = document.createElement('input');
            methodField.setAttribute('type', 'hidden');
            methodField.setAttribute('name', '_method');
            methodField.setAttribute('value', 'PUT');

            // Periksa apakah input _method sudah ada, jika ya, hapus
            var existingMethodField = document.getElementById('_method');
            if (existingMethodField) {
                existingMethodField.parentNode.removeChild(existingMethodField);
            }

            // Tambahkan input _method ke dalam formulir
            document.getElementById('categoryForm').appendChild(methodField);
        }
    </script>
@endsection
