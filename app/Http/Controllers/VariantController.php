<?php

namespace App\Http\Controllers;

use App\Models\Variant;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreVariantRequest;
use App\Http\Requests\UpdateVariantRequest;

class VariantController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $variants = Variant::paginate(5);
        return view('variants.index', compact('variants'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreVariantRequest $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'size' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->route('variants.index')->withErrors($validator)->withInput();
        }

        Variant::create($request->all());

        return redirect()->route('variants.index')->with('success', 'Variant created successfully');
    }

    /**
     * Display the specified resource.
     */
    public function show(Variant $variant)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Variant $variant)
    {
        // 
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateVariantRequest $request, Variant $variant)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|string',
            'size' => 'required|string',
        ]);

        if ($validator->fails()) {
            return redirect()->route('variants.index', $variant)->withErrors($validator)->withInput();
        }

        $variant->update($request->all());

        return redirect()->route('variants.index')->with('success', 'Variant updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Variant $variant)
    {
        $variant->delete();

        return redirect()->route('variants.index')->with('success', 'Variant deleted successfully');
    }
}
