<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Variant;
use Illuminate\Support\Str;
use App\Models\ProductImage;
use App\Models\ProductCategory;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $products = Product::paginate(5);
        $variants = Variant::all();
        $categories = ProductCategory::all();
        $images = ProductImage::all();
        return view('products.index', compact('products', 'variants', 'categories', 'images'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        // 
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreProductRequest $request)
    {
        Validator::make($request->all(), [
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'variant_id' => 'nullable|exists:variants,id',
            'category_id' => 'nullable|exists:product_categories,id',
            'url' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:5120', // 5 MB
        ]);

        try {
            $product = Product::create([
                'name' => $request->input('name'),
                'description' => $request->input('description'),
                'price' => $request->input('price'),
                'stock' => $request->input('stock'),
                'variant_id' => $request->input('variant_id'),
                'category_id' => $request->input('category_id'),
            ]);

            if ($request->hasFile('image')) {
                $imagePath = $request->file('image')->store('product_images', 'public');
                ProductImage::create([
                    'product_id' => $product->id,
                    'product_image' => $imagePath,
                ]);
            }

            return redirect()->route('products.index')->with('success', 'Product created successfully');
        } catch (\Exception $e) {
            // Tangani kesalahan lainnya di sini, jika diperlukan
            return view('products.index')->with('error', 'Failed to create product. ' . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Product $product)
    {
        return view('products.show', compact('product'));
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Product $product)
    {
        return view('products.edit', compact('product'));
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateProductRequest $request, Product $product)
    {
        // Validate the request
        Validator::make($request->all(), [
            'productId' => 'required|exists:products,id',
            'name' => 'required|string',
            'description' => 'required|string',
            'price' => 'required|integer',
            'stock' => 'required|integer',
            'variant_id' => 'nullable|exists:variants,id',
            'category_id' => 'nullable|exists:product_categories,id',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:5120', // 5 MB
        ]);

        // if ($validator->fails()) {
        //     return redirect()->route('products.index')->withErrors($validator)->withInput();
        // }

        // Find the product
        $product = Product::findOrFail($request->input('productId'));

        // Update the product
        $product->update([
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'price' => $request->input('price'),
            'stock' => $request->input('stock'),
            'variant_id' => $request->input('variant_id'),
            'category_id' => $request->input('category_id'),
        ]);

        // Handle image upload
        if ($request->hasFile('image')) {
            // Delete the old image if it exists
            if ($product->image) {
                Storage::disk('public')->delete($product->image->product_image);
                $product->image->delete();
            }

            // Store the new image
            $imagePath = $request->file('image')->store('product_images', 'public');
            ProductImage::create([
                'product_id' => $product->id,
                'product_image' => $imagePath,
            ]);
        }

        return redirect()->route('products.index')->with('success', 'Product updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Product $product)
    {
        // Hapus gambar terkait
        if ($product->image) {
            Storage::disk('public')->delete($product->image->product_image);
            $product->image->delete();
        }

        // Hapus produk
        $product->delete();

        return redirect()->route('products.index')->with('success', 'Product deleted successfully');
    }
}
