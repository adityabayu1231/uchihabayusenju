<?php

namespace App\Models;

use App\Models\Variant;
use App\Models\ProductImage;
use App\Models\ProductCategory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Product extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'description', 'price', 'stock', 'variant_id', 'category_id', 'image_id'];

    public function variant()
    {
        return $this->belongsTo(Variant::class);
    }

    public function category()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function image()
    {
        return $this->hasOne(ProductImage::class);
    }
}
